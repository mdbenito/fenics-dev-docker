#!/bin/bash
#
# This script sets up remote branch tracking to my private repos and
# checks out commits known to work with the development branch, as of
# 5.07.2017.
#

cd $FENICS_SRC_DIR

## Add a couple of forks

echo "**** fenics book ****"
git clone git@bitbucket.org:mdbenito/fenics-book-fork.git fenics-book
cd fenics-book
git remote add upstream https://mdbenito@bitbucket.org/fenics-project/fenics-book.git
git fetch
git checkout mdbenito/fix-typos
cd ..

echo "**** FFC ****"
cd ffc
git remote rename origin upstream
git remote add origin git@bitbucket.org:mdbenito/ffc-fork.git
git fetch origin
git checkout mdbenito/hermite
cd ..

echo "**** FIAT ****"
cd fiat
git remote rename origin upstream
git remote add origin git@bitbucket.org:mdbenito/fiat-fork.git
git fetch origin
git checkout mdbenito/hermite
cd ..

echo "**** UFL ****"
cd ufl
git remote rename origin upstream
git remote add origin git@bitbucket.org:mdbenito/ufl-fork.git
git fetch origin
git checkout mdbenito/hermite
cd ..

echo "**** mshr ****"
cd mshr
git remote rename origin upstream
git remote add origin git@bitbucket.org:mdbenito/mshr-fork.git
git checkout tags/2017.1.0
cd ..

# I don't have forks of the following projects:

echo "**** dolfin ****"
cd dolfin
git checkout tags/2017.1.0

echo "**** dijitso ****"
cd ../dijitso
git checkout tags/2017.1.0

echo "**** instant ****"
cd ../instant
git checkout tags/2017.1.0

cd ..

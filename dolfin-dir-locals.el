((c++-mode .
           ((cmake-ide-rdm-rc-path . "/usr/local/bin")
            (cmake-ide-cmake-command . "/usr/bin/cmake")
            (cmake-ide-make-command . "/usr/bin/make")
            (cmake-ide-build-pool-dir . "/home/fenics/local/builds")))
 (c-mode .
         ((cmake-ide-rdm-rc-path . "/usr/local/bin")
          (cmake-ide-cmake-command . "/usr/bin/cmake")
          (cmake-ide-make-command . "/usr/bin/make")
          (cmake-ide-build-pool-dir . "/home/fenics/local/builds"))))
